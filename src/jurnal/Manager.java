/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jurnal;

import jurnal.Employee;

/**
 *
 * @author ZANDUT
 */
public class Manager extends Employee
{

    private int type;

    public Manager(int type, String name)
    {
        super(name);
        setType(type);
    }

    private void setType(int type)
    {
        if (type >= 1 && type <= 2)
        {
            this.type = type;
        }
    }

    public int getType()
    {
        return type;
    }

    @Override
    public void setSalary(double salary)
    {
        if (type == 1)
        {
            super.salary = 1.25 * salary;
        } else if (type == 2)
        {
            super.salary = 1.5 * salary;
        } else
        {
            super.salary = salary;
        }
    }

}
